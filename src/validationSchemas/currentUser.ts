import { ValidationSchema } from 'fastest-validator';
export const CurrentUserSchema: ValidationSchema = {
    id: { type: 'string' },
    email: {type: 'string',min: 5, max: 100 },
    role: { type: 'string' },
    nickname: { type: 'string', min: 3, max: 50 },
    avatar: { type: 'string', optional: true, nullable: true, max: 400 },
}
