import {SafeUser} from "../../actions";
import {Comment, CommentWithUser} from "../article";

export interface Report {
    id: string;
    commentId: string;
    userId: string;
    comment: Comment;
    createdAt: Date;
    updatedAt?: Date;
}

export interface ReportWithUser extends Report{
    user?: SafeUser;
    comment: CommentWithUser;
}
