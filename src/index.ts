import {SafeUser} from "./actions";
import {FileArray} from "express-fileupload";

export * from './actions';
export * from './interfaces';
export * from './validationSchemas';

export enum Language {
    Uk = 'uk',
    En = 'en'
}

export enum ServiceName {
    Gateway = 'gateway',
    TelegramBot = 'telegramBot',
    Simbad = 'simbad',
    Article = 'article',
    Users = 'users',
    Files = 'files'
}

export enum Role {
    GUEST = 'guest',
    USER = 'user',
    AUTHOR = 'author',
    MODERATOR = 'moderator',
    ADMIN = 'admin'
}

export interface AuthorizedActionPayload {
    currentUser: SafeUser
}

export interface ActionPayload {
    currentUser?: SafeUser,
    files?: FileArray;
}

export interface ActionResult {
    errorMessage?: string;
    currentUser?: SafeUser;
}
