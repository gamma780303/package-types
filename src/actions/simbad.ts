import {ArticleWithUsers} from "../interfaces";

export enum SimbadActionName {
    GetObjectById = 'GetObjectById'
}

export interface GetObjectByIdPayload {
    identifier: string;
}

export interface GetObjectByIdResult {
    object: {
        [key: string]: string | string[];
    }
    articles: ArticleWithUsers[];
}
