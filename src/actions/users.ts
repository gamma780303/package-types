import {ActionPayload, ActionResult, AuthorizedActionPayload, Role} from "../index";
import {FileArray, UploadedFile} from "express-fileupload";

export enum UsersActionName {
    Auth = 'Auth',
    Login = 'Login',
    Register = 'Register',
    Refresh = 'Refresh',
    GetUser = 'GetUser',
    GetUserByEmail = 'GetUserByEmail',
    GetUsers = 'GetUsers',
    ChangeAvatar = 'ChangeAvatar',
    ChangeNickname = 'ChangeNickname',
    ChangePassword = 'ChangePassword',
    ResetPassword = 'ResetPassword',
    ClearTokens = 'ClearTokens',
    DeleteUser = 'DeleteUser',
    GetUsersByRole = 'GetUsersByRole',
    ChangeRole = 'ChangeRole'
}

export interface AuthPayload {
    access: string;
}

export interface AuthResult extends ActionResult {

}

export interface LoginPayload {
    email: string;
    password: string;
}

export interface LoginResult extends ActionResult {
    success: boolean;
    tokens?: {
        access: string;
        refresh: string;
    }
}

export interface RegisterPayload extends ActionPayload{
    email: string;
    nickname: string;
    password: string;
    avatar?: UploadedFile;
}

export interface RegisterResult extends ActionResult {
    success: boolean;
    message?: string;
}

export interface RefreshPayload {
    refresh: string;
}

export interface RefreshResult extends ActionResult {
    success: boolean;
    tokens?: {
        access: string;
        refresh: string;
    }
}

export interface GetUserPayload extends ActionPayload {
    userId: string;
}

export interface SafeUser {
    id: string;
    email: string;
    nickname: string;
    avatar?: string;
    role: string;
    createdAt: Date;
}

export interface User {
    id: string;
    name: string;
    nickname: string;
    avatar?: string;
    role: string;
    createdAt: Date;
    password: string;
}

export interface GetUserResult extends ActionResult {
    user?: SafeUser
}

export interface GetUserByEmailPayload {
    email: string;
}

export interface GetUsersPayload extends ActionPayload {
    userIds: string[];
}

export interface GetUsersResult extends ActionResult {
    users: SafeUser[];
}

export interface ChangeAvatarPayload extends AuthorizedActionPayload {
    avatar: UploadedFile;
}

export interface ChangeAvatarResult extends ActionResult {
    avatar: string;
}

export interface ChangeNicknamePayload extends AuthorizedActionPayload {
    nickname: string;
}

export interface ChangeNicknameResult extends ActionResult {
    nickname: string;
}

export interface ChangePasswordPayload extends AuthorizedActionPayload {
    currentPassword: string
    newPassword: string;
}

export interface ChangePasswordResult extends ActionResult {
    success: boolean;
}

export interface ResetPasswordPayload extends ActionPayload {
    email?: string;
}

export interface ResetPasswordResult extends ActionResult {

}

export interface ClearTokensPayload extends ActionPayload {

}

export interface ClearTokensResult extends ActionResult {
    success: boolean;
}

export interface DeleteUserPayload extends AuthorizedActionPayload {
    userId: string;
}

export interface DeleteUserResult extends ActionResult {
    success: boolean;
}

export interface GetUsersByRolePayload extends AuthorizedActionPayload {
    nickname?: string;
    role: string;
}

export interface GetUsersByRoleResult extends ActionResult {
    users: SafeUser[];
}

export interface ChangeRolePayload extends AuthorizedActionPayload {
    userId: string;
    role: string;
}

export interface ChangeRoleResult extends ActionResult {
    success: boolean;
}
