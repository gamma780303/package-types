"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UsersActionName = void 0;
var UsersActionName;
(function (UsersActionName) {
    UsersActionName["Auth"] = "Auth";
    UsersActionName["Login"] = "Login";
    UsersActionName["Register"] = "Register";
    UsersActionName["Refresh"] = "Refresh";
    UsersActionName["GetUser"] = "GetUser";
    UsersActionName["GetUserByEmail"] = "GetUserByEmail";
    UsersActionName["GetUsers"] = "GetUsers";
    UsersActionName["ChangeAvatar"] = "ChangeAvatar";
    UsersActionName["ChangeNickname"] = "ChangeNickname";
    UsersActionName["ChangePassword"] = "ChangePassword";
    UsersActionName["ResetPassword"] = "ResetPassword";
    UsersActionName["ClearTokens"] = "ClearTokens";
    UsersActionName["DeleteUser"] = "DeleteUser";
    UsersActionName["GetUsersByRole"] = "GetUsersByRole";
    UsersActionName["ChangeRole"] = "ChangeRole";
})(UsersActionName = exports.UsersActionName || (exports.UsersActionName = {}));
//# sourceMappingURL=users.js.map