/// <reference types="node" />
import { UploadedFile } from 'express-fileupload';
export declare enum FilesActionName {
    UploadFiles = "UploadFiles",
    RemoveFiles = "RemoveFiles",
    UploadBuffers = "UploadBuffers"
}
export interface UploadFilesPayload {
    files: {
        [key: string]: UploadedFile;
    };
}
export interface UploadFilesResult {
    fileKeys: {
        [key: string]: string;
    };
}
export interface RemoveFilesPayload {
    key: string;
}
export interface RemoveFilesResult {
    status: boolean;
}
export interface UploadBuffersPayload {
    buffers: {
        [key: string]: Buffer;
    };
}
export interface UploadBuffersResult {
    fileKeys: {
        [key: string]: string;
    };
}
