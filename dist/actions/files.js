"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.FilesActionName = void 0;
var FilesActionName;
(function (FilesActionName) {
    FilesActionName["UploadFiles"] = "UploadFiles";
    FilesActionName["RemoveFiles"] = "RemoveFiles";
    FilesActionName["UploadBuffers"] = "UploadBuffers";
})(FilesActionName = exports.FilesActionName || (exports.FilesActionName = {}));
//# sourceMappingURL=files.js.map