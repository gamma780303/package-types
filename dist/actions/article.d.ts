import { ActionPayload, ActionResult, AuthorizedActionPayload, CommentWithUser, RemarkWithUser, ReportWithUser } from "../index";
import { Comment } from "../interfaces/article/comment";
import { ArticleWithUsers } from "../interfaces/article/article";
import { UploadedFile } from "express-fileupload";
import { Remark } from "../interfaces/remark/remark";
export declare enum ArticleActionName {
    RequestAddArticle = "RequestAddArticle",
    ApproveArticle = "ApproveArticle",
    RejectArticle = "RejectArticle",
    GetRemarks = "GetRemarks",
    GetArticle = "GetArticle",
    GetArticles = "GetArticles",
    GetArticlesByAuthor = "GetArticlesByAuthor",
    GetArticlesForReview = "GetArticlesForReview",
    DeleteArticle = "DeleteArticle",
    EditArticle = "EditArticle",
    AddComment = "AddComment",
    GetComment = "GetComment",
    GetComments = "GetComments",
    GetCommentsByAuthor = "GetCommentsByAuthor",
    DeleteComment = "DeleteComment",
    EditComment = "EditComment",
    AddReport = "AddReport",
    GetReports = "GetReports",
    DeleteReport = "DeleteReport"
}
export interface GetArticlePayload extends ActionPayload {
    id: string;
}
export interface GetArticleResult extends ActionResult {
    article: ArticleWithUsers | null;
}
export interface SearchArticlesPayload extends ActionPayload {
    text?: string;
    orderBy?: 'createdAt' | 'viewCount';
    order?: 'DESC' | 'ASC';
    page?: number;
}
export interface SearchArticlesResult extends ActionResult {
    articles: ArticleWithUsers[];
}
export interface RequestAddArticlePayload extends AuthorizedActionPayload {
    title: string;
    text: string;
    tags?: string[];
    image: UploadedFile;
}
export interface RequestAddArticleResult extends ActionResult {
    article: ArticleWithUsers;
}
export interface GetArticlesForReviewPayload extends ActionPayload {
    title?: string;
    authorName?: string;
    page?: number;
}
export interface GetArticlesForReviewResult extends ActionResult {
    articles: ArticleWithUsers[];
}
export interface GetArticlesByAuthorPayload extends ActionPayload {
    authorId: string;
    page?: number;
}
export interface GetArticlesByAuthorResult extends ActionResult {
    articles: ArticleWithUsers[];
}
export interface ApproveArticlePayload extends AuthorizedActionPayload {
    articleId: string;
}
export interface ApproveArticleResult extends ActionResult {
    success: boolean;
}
export interface RejectArticlePayload extends AuthorizedActionPayload {
    articleId: string;
    remarks: string[] | string;
}
export interface EditArticlePayload extends AuthorizedActionPayload {
    articleId: string;
    title: string;
    text: string;
    tags?: string[];
    image: UploadedFile | string;
}
export interface EditArticleResult extends ActionResult {
    success: boolean;
}
export interface RejectArticleResult extends ActionResult {
    remarks: Remark[];
}
export interface GetRemarksPayload extends AuthorizedActionPayload {
    articleId: string;
}
export interface GetRemarksResult extends ActionResult {
    remarks: RemarkWithUser[];
}
export interface AddCommentPayload extends AuthorizedActionPayload {
    articleId: string;
    text: string;
}
export interface AddCommentResult extends ActionResult {
    comment?: CommentWithUser;
}
export interface GetCommentsPayload extends ActionPayload {
    articleId: string;
}
export interface GetCommentsResult extends ActionResult {
    comments: CommentWithUser[];
}
export interface GetCommentsByAuthorPayload extends ActionPayload {
    authorId: string;
}
export interface GetCommentsByAuthorResult extends ActionResult {
    comments: Comment[];
}
export interface DeleteCommentPayload extends AuthorizedActionPayload {
    commentId: string;
}
export interface DeleteCommentResult extends ActionResult {
    success: boolean;
}
export interface GetReportsPayload extends AuthorizedActionPayload {
    page: number;
}
export interface GetReportsResult extends ActionResult {
    reports: ReportWithUser[];
}
export interface AddReportPayload extends AuthorizedActionPayload {
    commentId: string;
}
export interface AddReportResult extends ActionResult {
    success: boolean;
}
export interface DeleteReportPayload extends AuthorizedActionPayload {
    reportId: string;
}
export interface DeleteReportResult extends ActionResult {
    success: boolean;
}
export interface DeleteArticlePayload extends AuthorizedActionPayload {
    articleId: string;
}
export interface DeleteArticleResult extends ActionResult {
    success: boolean;
}
