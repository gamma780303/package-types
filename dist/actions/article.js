"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ArticleActionName = void 0;
var ArticleActionName;
(function (ArticleActionName) {
    ArticleActionName["RequestAddArticle"] = "RequestAddArticle";
    ArticleActionName["ApproveArticle"] = "ApproveArticle";
    ArticleActionName["RejectArticle"] = "RejectArticle";
    ArticleActionName["GetRemarks"] = "GetRemarks";
    ArticleActionName["GetArticle"] = "GetArticle";
    ArticleActionName["GetArticles"] = "GetArticles";
    ArticleActionName["GetArticlesByAuthor"] = "GetArticlesByAuthor";
    ArticleActionName["GetArticlesForReview"] = "GetArticlesForReview";
    ArticleActionName["DeleteArticle"] = "DeleteArticle";
    ArticleActionName["EditArticle"] = "EditArticle";
    ArticleActionName["AddComment"] = "AddComment";
    ArticleActionName["GetComment"] = "GetComment";
    ArticleActionName["GetComments"] = "GetComments";
    ArticleActionName["GetCommentsByAuthor"] = "GetCommentsByAuthor";
    ArticleActionName["DeleteComment"] = "DeleteComment";
    ArticleActionName["EditComment"] = "EditComment";
    ArticleActionName["AddReport"] = "AddReport";
    ArticleActionName["GetReports"] = "GetReports";
    ArticleActionName["DeleteReport"] = "DeleteReport";
})(ArticleActionName = exports.ArticleActionName || (exports.ArticleActionName = {}));
//# sourceMappingURL=article.js.map