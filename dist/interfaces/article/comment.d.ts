import { SafeUser } from "../../actions";
export interface Comment {
    id: string;
    articleId: string;
    authorId: string;
    text: string;
    createdAt: Date;
    updatedAt?: Date;
}
export interface CommentWithUser extends Comment {
    author?: SafeUser;
}
