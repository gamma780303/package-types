import { SafeUser } from "../../actions";
import { CommentWithUser } from "./comment";
export interface Article {
    id: string;
    title: string;
    text: string;
    tags?: string[];
    authorId: string;
    posted: boolean;
    comments?: CommentWithUser[];
    reviewerId?: string;
    firstPublishedAt?: Date;
    createdAt: Date;
    updatedAt?: Date;
    image: string;
    viewCount: number;
}
export interface ArticleWithUsers extends Article {
    author?: SafeUser;
    reviewer?: SafeUser;
}
