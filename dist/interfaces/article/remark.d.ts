export interface remark {
    id: string;
    articleId: string;
    text: string;
    createdAt: Date;
    updatedAt: Date;
}
