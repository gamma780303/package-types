import { SafeUser } from "../../actions";
export interface Remark {
    id: string;
    authorId: string;
    articleId: string;
    text: string;
}
export interface RemarkWithUser extends Remark {
    author?: SafeUser;
}
