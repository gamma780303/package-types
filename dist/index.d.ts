import { SafeUser } from "./actions";
import { FileArray } from "express-fileupload";
export * from './actions';
export * from './interfaces';
export * from './validationSchemas';
export declare enum Language {
    Uk = "uk",
    En = "en"
}
export declare enum ServiceName {
    Gateway = "gateway",
    TelegramBot = "telegramBot",
    Simbad = "simbad",
    Article = "article",
    Users = "users",
    Files = "files"
}
export declare enum Role {
    GUEST = "guest",
    USER = "user",
    AUTHOR = "author",
    MODERATOR = "moderator",
    ADMIN = "admin"
}
export interface AuthorizedActionPayload {
    currentUser: SafeUser;
}
export interface ActionPayload {
    currentUser?: SafeUser;
    files?: FileArray;
}
export interface ActionResult {
    errorMessage?: string;
    currentUser?: SafeUser;
}
