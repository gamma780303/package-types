"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CurrentUserSchema = void 0;
exports.CurrentUserSchema = {
    id: { type: 'string' },
    email: { type: 'string', min: 5, max: 100 },
    role: { type: 'string' },
    nickname: { type: 'string', min: 3, max: 50 },
    avatar: { type: 'string', optional: true, nullable: true, max: 400 },
};
//# sourceMappingURL=currentUser.js.map